﻿using System.Collections.Generic;
using System.IO;
using System.Timers;
using System.Xml.Serialization;
using Storebase.args;
using Storebase.net;
using Storebase.store;
using Storekeeper.serviceworker;
using ConHost = Storebase.net.connectivity.ConHost;

namespace Storekeeper {
    internal static partial class Program {
        internal static List<ClientServiceWorker> Clients = new List<ClientServiceWorker>();
        
        internal static int ClientTimeout;
        internal static int MaxSimultaneousFileTransfers;
        
        private static string _bucketCache;
        private static string _certificatePath;

        public static void Main(string[] args) {
            var port = new Argument<int>("port", "p").Fetch(args, 62822);
            ClientTimeout = new Argument<int>("timeout", "t").Fetch(args, 5000);
            MaxSimultaneousFileTransfers = new Argument<int>("max-transfers", "mt").Fetch(args, 8);
            var loopTime = new Argument<int>("loop-time", "lt").Fetch(args, 1000);
            _bucketCache = new Argument<string>("bucket-cache", "bc").Fetch(args, "buckets.cache");
            _certificatePath = new Argument<string>("cert-path", "cp").Fetch(args, "Storekeeper.pfx");

            var ser = new XmlSerializer(Buckets.GetType());
            if (File.Exists(_bucketCache)) {
                var str = File.OpenRead(_bucketCache);
                Buckets = (List<StorageBucket>) ser.Deserialize(str);
                str.Close();
                
                Buckets.ForEach(bucket => bucket.PostDeserialize());
            }
            
            var con = new ConHost(_certificatePath, port);
            con.OnConnect += (_, c) => {
                Clients.Add(new ClientServiceWorker(c));
            };

            con.Start();

            var t = new Timer(loopTime);
            t.Elapsed += Loop;
            t.Start();
        }
    }
}