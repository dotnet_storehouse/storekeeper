﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Storebase.net;
using Storebase.net.@base;
using Storebase.net.messageloop;
using Storebase.store;

namespace Storekeeper.handler {
    public class BucketPushHandler : IMessageHandler {
        public Signal.Type HandeledType => Signal.Type.Control;
        public Signal.Codes HandeledCode => Signal.Codes.BucketPush;
        
        public void OnMessage(ref Message m, BasicMessageLoop loop) {
            var ser = new XmlSerializer(typeof(StorageBucket));
            var str = new MemoryStream(m.Request.Data.Skip(1).ToArray());

            var b = (StorageBucket) ser.Deserialize(str);

            Packet p;
            if (Program.Buckets.Count(bckt => bckt.Guid.Equals(b.Guid)) > 0) {
                p = new Packet(Signal.Type.Control, Signal.Codes.ErrorDuplicateBucket);
                m.Response = p;
            }

            Console.WriteLine("Received bucket '" + b + "'");
            b.Path = "{" + b.Guid + "}";
            Program.Buckets.Add(b);
            b.EnableFsw(true);
            
            p = new Packet(Signal.Type.Control, Signal.Codes.Ack);
            m.Response = p;

            Program.BucketsDirty = true;

        }
    }
}