﻿using System.IO;
using System.Xml.Serialization;
using Storebase.net;
using Storebase.net.@base;
using Storebase.net.messageloop;

namespace Storekeeper.handler {
    public class BucketListHandler : IMessageHandler {
        public Signal.Type HandeledType => Signal.Type.Control;
        public Signal.Codes HandeledCode => Signal.Codes.BucketList;
        
        public void OnMessage(ref Message m, BasicMessageLoop loop) {
            var ser = new XmlSerializer(Program.Buckets.GetType());
            var str = new MemoryStream();
            ser.Serialize(str, Program.Buckets);
            m.Response = new Packet(Signal.Type.Data, str.ToArray());
        }
    }
}