﻿using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Storebase.net;
using Storebase.net.@base;
using Storebase.net.messageloop;

namespace Storekeeper.handler {
    public class BucketFetchHandler : IMessageHandler {
        public Signal.Type HandeledType => Signal.Type.Control;
        public Signal.Codes HandeledCode => Signal.Codes.BucketFetch;
        
        public void OnMessage(ref Message m, BasicMessageLoop loop) {
            var bs = m.Request.Data.Skip(1).ToArray();
            var guid = Encoding.Unicode.GetString(bs);

            var dat = new[] { (byte) Signal.Codes.Error};
            var bl = Program.Buckets.Where(bckt => bckt.Guid.ToString().StartsWith(guid)).ToList();
            if (bl.Count < 1)
                dat[0] = (byte) Signal.Codes.ErrorBucketNotFound;
            else if (bl.Count > 1)
                dat[0] = (byte) Signal.Codes.ErrorAmbiguousGuid;
            else {
                var ser = new XmlSerializer(bl[0].GetType());
                var str = new MemoryStream();
                ser.Serialize(str, bl[0]);
                dat = new[] {(byte) Signal.Codes.Ack}.Concat(str.ToArray()).ToArray();
            }
            
            m.Response = new Packet(Signal.Type.Data, dat);
        }
    }
}