﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Storebase.net;
using Storebase.net.@base;
using Storebase.net.messageloop;

namespace Storekeeper.handler {
    public class BucketGetHandler : IMessageHandler {
        public Signal.Type HandeledType => Signal.Type.Control;
        public Signal.Codes HandeledCode => Signal.Codes.BucketGet;

        public void OnMessage(ref Message m, BasicMessageLoop loop) {
            var guid = new Guid(m.Request.Data.Skip(1).ToArray());
            var bckt = Program.Buckets.FirstOrDefault(b => b.Guid.Equals(guid));
            if (bckt == null) {
                m.Response = new Packet(Signal.Type.Control, Signal.Codes.ErrorBucketNotFound);
                return;
            }

            bckt.CheckOnceForChanges(true);
            var ser = new XmlSerializer(bckt.GetType());
            var str = new MemoryStream();
            ser.Serialize(str, bckt);
            m.Response = new Packet(Signal.Type.Data,
                (new[] {(byte) Signal.Codes.Ack}).Concat(str.ToArray()).ToArray());
        }
    }
}