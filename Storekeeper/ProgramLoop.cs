﻿using System.IO;
using System.Linq;
using System.Timers;
using System.Xml.Serialization;

namespace Storekeeper {
    internal static partial class Program {
        internal static bool BucketsDirty;

        private static void Loop(object sender, ElapsedEventArgs elapsedEventArgs) {
            if (BucketsDirty) {
                var str = File.OpenWrite(_bucketCache);
                var ser = new XmlSerializer(Buckets.GetType());
                ser.Serialize(str, Buckets);
                str.Flush();
                str.Close();

                BucketsDirty = false;
            }

            if (Clients.Sum(worker => worker.ActiveFileTransfers.Count) < MaxSimultaneousFileTransfers &&
                Clients.Sum(worker => worker.PendingFileTransfers.Count) > 0) {
                var client = Clients.First(worker => worker.PendingFileTransfers.Count > 0);
                var fth = client.PendingFileTransfers.First();

                client.StartFileTransfer(fth.Index);
            }
        }
    }
}