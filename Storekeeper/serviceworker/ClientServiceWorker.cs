﻿using System;
using System.Collections.Generic;
using Storebase.net;
using Storebase.net.@base;
using Storebase.net.messageloop;
using Storebase.net.messageloop.handler;
using Storebase.store;
using Storekeeper.handler;

namespace Storekeeper.serviceworker {
    public class ClientServiceWorker : StorehouseMessageLoop {

        private HeartBeatSender _hbs;
        
        public ClientServiceWorker(ITcpClient client) : base(client, true) {
            Console.WriteLine($"New client: {client.Hostname}");
            
            MessageHandlers.Add(new BucketListHandler());
            MessageHandlers.Add(new BucketPushHandler());
            MessageHandlers.Add(new BucketFetchHandler());
            MessageHandlers.Add(new BucketGetHandler());
            
            Start();
            
            _hbs = new HeartBeatSender(this);
            _hbs.Start();
            
            PostConstruct();
        }

        protected override void OnDisconnect() {
            Console.WriteLine($"Client disconnected: {Client.Hostname}");
            Program.Clients.Remove(this);
        }

        public override List<StorageBucket> GetBucketList() { return Program.Buckets; }
    }
}